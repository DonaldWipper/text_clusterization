'''
7. (9-20 баллов)
Реализовать автоматическую оценку тональности русскоязычных текстов по отношению к
какому-то объекту (лицу, продукту, товару и т.п.) путем классификации текстов на 2
класса (положительный/отрицательный). Использовать либо метод машинного обучения
(например, SVM) и заранее размеченную коллекцию текстов, или же специальные словари
оценочных слов и правила (шаблонов).
В отчет включить описание использованного метода оценки тональности
#https://habrahabr.ru/post/263171/


8. (9-20 баллов)
Составить программу, которая используя API к одной из поисковых систем, например:
 Сервис Яндекс.XML, позволяющий отправлять запросы к поисковой базе Яндекса и
получать ответы в формате XML (https://tech.yandex.ru/xml/)
 Google Custom Search (https://developers.google.com/custom-search/)
получает тексты ответов (сниппетов) на заданный запрос (или же извлекает сами текстовые
документы) и разбивает множество полученных текстов на кластеры, применяя один из
библиотечных методов кластеризации. Рассмотреть несколько (3-5) различных запросов и
соответствующую выдачу поисковиком 20-50 текстов, проанализировать результаты
кластеризации.
В отчет дополнительно включить примененный метод кластеризации, примеры запросов к
поисковику, характеристику полученных кластеров.
'''
#визуализаяция на матлабе 
#http://blog.esemi.ru/2012/08/k-means-python.html
#https://radimrehurek.com/gensim/models/word2vec.html
#https://stackoverflow.com/questions/1789254/clustering-text-in-python#1789254
#http://datareview.info/article/klasterizatsiya-s-pomoshhyu-metoda-k-srednih-na-python/
#https://datasciencelab.wordpress.com/2013/12/12/clustering-with-k-means-in-python/
#http://brandonrose.org/clustering

import xml.etree.ElementTree as ET
import requests
import numpy as np
import pandas as pd
import nltk
import re
import os
import codecs
from sklearn import feature_extraction
from sklearn.feature_extraction.text import TfidfVectorizer
import mpld3
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.cluster import KMeans

texts = []

yandex_login = "utiralov"
yandex_key = "03.23308875:21d1d8c85049f1384f7bb33b3d6e1191"

docs_on_pages = 100 #документов на странице
docs_in_group = 1  #документа в группе
filter = "none"
sort_by = "rlv"

#https://xml.yandex.ru/settings/ настройки

# load nltk's SnowballStemmer as variabled 'stemmer'
from nltk.stem.snowball import SnowballStemmer
stemmer = SnowballStemmer("russian")


def limits_info():
    query = "https://yandex.com/search/xml?l10n=en&action=limits-info&user=" + yandex_login + "&key=" + yandex_key 
    res = requests.get(query).text
    return res

def search_in_yandex(question):
    query = "https://yandex.com/search/xml?" 
    query += "&user=" + yandex_login 
    query += "&key="  + yandex_key
    query += "&query=" + question + "&l10n=en"
    query += "&sortby=" + sort_by
    query += "&filter=" + filter
    query += "&groupby=attr%3Dd.mode%3Ddeep.groups-on-page%3D" + str(docs_on_pages) + "docs-in-group%3D" + str(docs_in_group)
    print(query)

    #query += "&groupby=" + "attr%3D%22%22.mode%3Dflat.groups-on-page%3D"  + str(docs_on_pages) +  ".docs-in-group%3D"  + str(docs_in_group)   
    res = requests.get(query).text
    return res


# here I define a tokenizer and stemmer which returns the set of stems in the text that it is passed

def tokenize_and_stem(text):
    # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
    tokens = [word for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
    filtered_tokens = []
    # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
    for token in tokens:
        if re.search('[a-яA-Я]', token):
            filtered_tokens.append(token)
    stems = [stemmer.stem(t) for t in filtered_tokens]
    return stems


def tokenize_only(text):
    # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
    tokens = [word.lower() for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
    filtered_tokens = []
    # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
    for token in tokens:
        if re.search('[a-яA-Я]', token):
            filtered_tokens.append(token)
    return filtered_tokens




def main():
    nltk.download('stopwords')
    nltk.download('punkt')
    stopwords = nltk.corpus.stopwords.words('russian')
    root = ET.fromstring(search_in_yandex("Родченков").replace("<hlword>", "").replace("</hlword>", ""))
    _snippets = root.findall("./response/results/grouping/group/doc/passages/passage")
    snippets = [s.text for s in _snippets]
    i = 0
    totalvocab_stemmed = []
    totalvocab_tokenized = []
    for snip in snippets:
        totalvocab_stemmed.extend(tokenize_and_stem(snip))
        totalvocab_tokenized.extend(tokenize_only(snip)) 
        i += 1 
   
    vocab_frame = pd.DataFrame({'words': totalvocab_tokenized}, index = totalvocab_stemmed)
    print ('there are ' + str(vocab_frame.shape[0]) + ' items in vocab_frame')   
    
    #define vectorizer parameters
    tfidf_vectorizer = TfidfVectorizer(max_df=0.8, max_features=200000,
                                 min_df=0.0, stop_words = stopwords,
                                 use_idf=True, tokenizer=tokenize_and_stem, ngram_range=(1,3))

    tfidf_matrix = tfidf_vectorizer.fit_transform(snippets) #fit the vectorizer to synopses
    print(tfidf_matrix.shape)
    terms = tfidf_vectorizer.get_feature_names()
    dist = 1 - cosine_similarity(tfidf_matrix)

    num_clusters = 5

    km = KMeans(n_clusters=num_clusters)
    km.fit(tfidf_matrix)
    clusters = km.labels_.tolist()
    
    print("Top terms per cluster:")
    print()
    #sort cluster centers by proximity to centroid
    order_centroids = km.cluster_centers_.argsort()[:, ::-1] 

    for i in range(num_clusters):
        print("Cluster %d words:" % i, end='')
    
        for ind in order_centroids[i, :6]: #replace 6 with n words per cluster
            print(' %s' % vocab_frame.ix[terms[ind].split(' ')].values.tolist()[0][0].encode('utf-8', 'ignore'), end=',')
        print() #add whitespace
        print() #add whitespace
        print("Cluster %d titles:" % i, end='')
        for title in frame.ix[i]['title'].values.tolist():
            print(' %s,' % title, end='')
        print() #add whitespace
        print() #add whitespace
    print()
    print() 

main()







